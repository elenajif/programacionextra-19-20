package principal;

import java.util.Scanner;

import clases.Garaje;

public class Principal {

	public static void main(String[] args) {
		Garaje unGaraje = new Garaje();
		unGaraje.cargarDatos();
		Scanner input = new Scanner(System.in);
		int opcion = 0;

		do {
			unGaraje.mostrarMenu();
			opcion = input.nextInt();

			switch (opcion) {
			case 1:
				unGaraje.listarPlazas();
				break;
			case 2: {
				System.out.println("Introduce un numero de plaza");
				int plaza = input.nextInt();
				input.nextLine();
				if (!unGaraje.isOcupada(plaza)) {
					System.out.println("Introduce un nombre");
					String nombre = input.nextLine();
					System.out.println("Introduce un dni");
					String dni = input.nextLine();
					System.out.println("Introduce fecha nacimiento (dd/mm/aaaa)");
					String fecha = input.nextLine();
					System.out.println("Introduce superficie plaza");
					double superficie = Double.parseDouble(input.nextLine());
					unGaraje.alquilarPlaza(plaza, nombre, dni, fecha, superficie);
				} else {
					System.out.println("la plaza esta ocupada");
				}
			}
				break;
			case 3:
				unGaraje.mostrarEstadoPlazas();
				break;
			case 4: {
				System.out.println("Introduce la plaza a eliminar");
				int plaza = input.nextInt();
				input.nextLine();
				if (unGaraje.isOcupada(plaza)) {
					unGaraje.eliminarAlquiler(plaza);
				} else {
					System.out.println("La plaza no esta ocupada");
				}
			}
				break;
			case 5:
				
				break;
			default:
				System.out.println("Opcion no contemplada");
			}
		} while (opcion != 5);
		input.close();
		
		unGaraje.guardarDatos();
	}

}
