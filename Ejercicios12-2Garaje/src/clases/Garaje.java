package clases;

public class Garaje {
	
	private Plaza[] arrayPlazas;
	private GestorFicheros gestor;
	
	public Garaje(){
		this.arrayPlazas = new Plaza[10];
		this.gestor = new GestorFicheros("datosGaraje.dat");
	}
	
	public Garaje(int num){
		this.arrayPlazas = new Plaza[num];
		this.gestor = new GestorFicheros("datosGaraje.dat");
	}
	
	public Garaje(String rutaDatos){
		this.arrayPlazas = new Plaza[10];
		this.gestor = new GestorFicheros(rutaDatos);
	}
	
	public Garaje(int num, String rutaDatos){
		this.arrayPlazas = new Plaza[num];
		this.gestor = new GestorFicheros(rutaDatos);
	}
	
	public void listarPlazas(){
		for(int i = 0; i < arrayPlazas.length; i++){
			if(arrayPlazas[i] != null){
				System.out.println("N� " + (i + 1) + " :" + arrayPlazas[i]);
			}
		}
	}
	
	public void alquilarPlaza(int numPlaza, String nombre, String dni, String fechaNacimiento, double superficie){
		Plaza nuevaPlaza = new Plaza(superficie, nombre, dni, fechaNacimiento);
		this.arrayPlazas[numPlaza - 1] = nuevaPlaza;
	}
	
	public void mostrarEstadoPlazas(){
		for(int i = 0; i < arrayPlazas.length; i++){
			if(arrayPlazas[i] != null){
				System.out.println("Plaza " + (i + 1) + " Ocupada");
			}else{
				System.out.println("Plaza " + (i + 1) + " Libre");
			}
		}
	}
	
	public void eliminarAlquiler(int numPlaza){
		arrayPlazas[numPlaza - 1] = null;
	}
	
	public boolean isOcupada(int numPlaza){
		if(arrayPlazas[numPlaza -1] == null){
			return false;
		}
		return true;
	}
	
	public int getCantidadPlazas(){
		return arrayPlazas.length;
	}
	
	public void mostrarMenu(){
		System.out.println("\n1 - Listar Plazas");
		System.out.println("2 - Alquilar Plaza");
		System.out.println("3 - Mostrar estado de plazas");
		System.out.println("4 - Eliminar Plaza");
		System.out.println("5 - Salir");
		System.out.println("Selecciona una opcion");
	}
	
	public void guardarDatos(){
		gestor.escribirArray(arrayPlazas);
	}
	
	public void cargarDatos(){
		this.arrayPlazas = gestor.cargarArray();
	}
	
}
