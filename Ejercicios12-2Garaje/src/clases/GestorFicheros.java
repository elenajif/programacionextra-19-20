package clases;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class GestorFicheros {

	private File fichero;
	
	public GestorFicheros(String rutaFichero){
		fichero = new File(rutaFichero);
	}
	
	public void escribirArray(Plaza[] array){
		PrintWriter escritor = null;
		DateTimeFormatter formateador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		try {
			escritor = new PrintWriter(fichero);
			
			for(int i = 0; i < array.length; i++){
				if(array[i] != null){
					escritor.println(i + "::" + array[i].getSuperficie() + "::" + array[i].getNombre() 
							+ "::" + array[i].getDni() + "::" + array[i].getFechaNacimiento()  
							+ "::" + formateador.format(array[i].getFechaReserva()));
				}
			}	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}finally{
			if(escritor != null){
				escritor.close();
			}
		}
		
		
		
	}
	
	public Plaza[] cargarArray(){
		Plaza[] array =  new Plaza[10];
		Scanner lector = null;
		
		try {
			lector = new Scanner(fichero);
			while(lector.hasNextLine()){
				String cadenaFormateada = lector.nextLine();
				String[] campos = cadenaFormateada.split("::");
				int posicion = Integer.parseInt(campos[0]);
				
				Plaza plaza = crearPlaza(campos);
				array[posicion] = plaza;
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}finally{
			if(lector != null){
				lector.close();
			}
		}
		
		return null;
	}

	private Plaza crearPlaza(String[] campos) {
		double superficie = Double.parseDouble(campos[1]);
		String nombre = campos[2];
		String dni = campos[3];
		String fechaNacimiento = campos[4];
		String fechaReserva = campos[5];
		
		Plaza nueva = new Plaza(superficie, nombre, dni, fechaNacimiento);
		nueva.setFechaReserva(LocalDateTime.parse(fechaReserva));
		return nueva;
	}
	
}
