package clases;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class Plaza {
	private double superficie;
	private LocalDateTime fechaReserva;
	private String nombre;
	private String dni;
	private LocalDate fechaNacimiento;
	
	public Plaza(double superficie, String nombre, String dni, String fechaNacimiento) {
		this.superficie = superficie;
		this.nombre = nombre;
		this.dni = dni;
		this.fechaReserva = LocalDateTime.now();
		//Creo un formateador de fechas para formato (dd/mm/aaaa)
		DateTimeFormatter formateador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		this.fechaNacimiento = LocalDate.parse(fechaNacimiento, formateador);
	}

	public double getSuperficie() {
		return superficie;
	}

	public void setSuperficie(double superficie) {
		this.superficie = superficie;
	}

	public LocalDateTime getFechaReserva() {
		return fechaReserva;
	}

	public void setFechaReserva(LocalDateTime fechaReserva) {
		this.fechaReserva = fechaReserva;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	public int getEdad(){
		Period periodo = Period.between(fechaNacimiento, LocalDate.now());
		return periodo.getYears();
	}
	
	@Override
	public String toString(){
		DateTimeFormatter formateador = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		return nombre + " - " + dni + "- Reserva: " + formateador.format(fechaReserva);
	}
	
	
	@Override
	public boolean equals(Object objeto) {
		//Si el valor que recibo no es null
		if(objeto == null){
			return false;
		}
		//Si el valor que recibo es un objeto de otra clase
		if(objeto.getClass() != Plaza.class){
			return false;
		}
		
		//Como el objeto es de clase plaza, casteo a tipo plaza
		Plaza nuevaPlaza = (Plaza) objeto;
		//Si algun campo no coincide, devuelvo false
		if(!this.dni.equals(nuevaPlaza.dni)
				|| !this.nombre.equals(nuevaPlaza.nombre)
				|| !this.fechaNacimiento.equals(nuevaPlaza.fechaNacimiento)
				|| !this.fechaReserva.equals(nuevaPlaza.fechaReserva)
				|| this.superficie != nuevaPlaza.superficie){
			return false;
		}
		
		//Si no, devuelvo true
		return true;
	}
	
}
